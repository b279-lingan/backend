// s28 activity

// Single Document

db.rooms.insertOne({
	name: "single",
	accommodates: 2,
	price: 1000,
	description: "A simple room with all the basic necessities",
	rooms_available: 10,
	isAvailable: false
});

// Many Documents

db.rooms.insertMany([
	{
		name: "double",
		accommodates: 3,
		price: 2000,
		description: "A room fit for a small family going on a vacation",
		rooms_available: 5,
		isAvailable: false
	},
	{
		name: "queen",
		accommodates: 4,
		price: 4000,
		description: "A room with a queen sized bed perfect for a simple getaway",
		rooms_available: 15,
		isAvailable: false
	}
]);


// Find Documents

db.rooms.find({ name: "double" });

// Update Documents

db.rooms.updateOne(
	{name: "queen" },
	{
		$set: {
			rooms_available : 0
		}
	}
);

// Deleting Documents

db.rooms.deleteOne({
	rooms_available: 0,
});