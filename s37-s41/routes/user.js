const express = require("express");
const router = express.Router();
const userController = require("../controllers/user.js")
const auth = require("../auth.js")


// route for checking if email already exist.
router.post("/checkEmail", (req, res) => {
    userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
})


router.post("/register", (req, res) =>{ 
    userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));

});


// route for user login
router.post("/login", (req, res) => {
    userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
});

// [ACTIVITY]
router.post("/details",auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController))
});

// Enroll user to a course
// router.post("/enroll", (req, res) => {
//     let data = {
//         userId : req.body.userId,
//         courseId : req.body.courseId
//     }
//     userController.enroll(data).then(resultFromController => res.send(resultFromController))
// });

// [ACTIVITY]
router.post("/enroll", (req, res) => {
    let data = {
        userId : auth.decode(req.headers.authorization).id,
        isAdmin : auth.decode(req.headers.authorization).isAdmin,
        courseId : req.body.courseId
    }
    userController.enroll(data).then(resultFromController => res.send(resultFromController))
});


// Allows us to export the "router" object that will be access in our index.js file.
module.exports = router;