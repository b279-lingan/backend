// 1. Total number of fruit on sale.

db.fruits.aggregate([
	{$match: {onSale: true }},
	{$group: {_id: "$supplier_id", enoughStock: {$sum: 1}}},
	{$project: {_id: 0}}
]);



// 2. Total number of fruits with stock more than or equal to 20.

db.fruits.aggregate([
	{$match: {stock: {$gte:20}}},
	{$group: {_id: "$supplier_id", enoughStock: {$sum: 1}}},
	{$project: {_id: 0}}
]);

// 3. Average price of fruits onSale per supplier.

db.fruits.aggregate([
	{$match:{onSale: true }},
	{$group:{_id: "$supplier_id", avg_price: {$avg: "$price"}}}
]);

// 4. Highest price of a fruit per supplier.

db.fruits.aggregate([
	{$match: {onSale: true}},
	{$group: {_id: "$supplier_id", max_price: {$max: "$price"}}},
	{$sort: {max_price: 1}}
]);

// 5. Lowest price of a fruit per supplier.

db.fruits.aggregate([
	{$match: {onSale: true}},
	{$group: {_id: "$supplier_id", min_price: {$min: "$price"}}},
	{$sort: {min_price: 1}}
]);
