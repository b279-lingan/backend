const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
	userId : {
        type : String,
        required : [true, "Please enter User Id!"]
    },
	products : [
        {
            productId : {
                type : String,
                required : [true, "Please enter Product Id!"]
            },
            quantity : {
                type : Number,
                required : [true, "Please enter Quantity!"]
            },
        }
    ],
	totalAmount : {
		type : Number,
		required : [true, "Please enter Total Amount!"]
	},
	purchasedOn : {
		type : Date,
		default : new Date()
	},
})

module.exports = mongoose.model("Order", orderSchema);