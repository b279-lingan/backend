const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	name : {
		type : String,
		required : [true, "Please enter Name!"]
	},
	email : {
		type : String,
		required : [true, "Please enter Email!"]
	},
	password : {
		type : String,
		required : [true, "Please enter Password!"]
	},
	isAdmin : {
		type : Boolean,
		default : false
	},
	mobileNo : {
		type : String, 
		required : [true, "Please enter Mobile Number"]
	},
})

module.exports = mongoose.model("User", userSchema);