const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	name : {
		type : String,
		required : [true, "Please enter Name!"]
	},
	description : {
		type : String,
		required : [true, "Please enter description!"]
	},
    price : {
		type : Number,
		required : [true, "Please enter Price!"]
	},
    isActive : {
		type : Boolean,
		default : true
	},
    createdOn : {
        type : Date,
        default : new Date()
    }
})

module.exports = mongoose.model("Product", productSchema);
