const auth = require("../auth.js")
const express = require("express");
const router = express.Router();
const orderController = require("../controllers/orderController.js");

/* ROUTER CHECKOUT */
router.post("/checkout", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	const data = {
		userId: userData.id,
		email: userData.email,
		reqBody: req.body
	}

	if(userData.isAdmin == false) {
		orderController.userCheckout(data).then(resultFromController => {
			res.send(resultFromController);
		});
	} else {
		res.send("Successfully checkout.");
	}
});

/* ROUTER VIEW ORDER */
router.get("/my-orders", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	const data = {
		userId: userData.id,
		email: userData.email,
	}
	if(userData.isAdmin == false) {
			orderController.myOrders(data).then(resultFromController => {
				res.send(resultFromController);
			});
		} else {
			res.send("User need to login");
		}
	});


module.exports = router;