const Order = require("../models/Order");
const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.userCheckout = (data) => {

    return Product.findById(data.reqBody.productId).then(product => {
        if(product == null) {

            return "Error! Try again!";
        
        } else {

            let newOrder = new Order({
                userId: data.userId,
                products: {
                    productId: data.reqBody.productId,
                    quantity: data.reqBody.quantity
                },
                totalAmount: product.price * data.reqBody.quantity
            });
            return newOrder.save().then((order, error) => {
                if (error) {
                    return false;
                } else {
                    return "Successfully Place an Order";
                }
            });
        }
    });
}

module.exports.myOrders = (data) => {
    return Order.find({ userId: data.userId }).then(result => {
        return result;
    });
};