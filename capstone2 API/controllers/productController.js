const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");

/* ADD PRODUCT */
module.exports.addProduct = (data) => {
	console.log(data.isAdmin);
	if(data.isAdmin){
		let newProduct = new Product({
			name: data.product.name,
			description: data.product.description,
			price: data.product.price
		})
		return newProduct.save().then((product, error) => {
			if(error) {
				return error;
			}
			return "Successfully add the Product!";
		})
	}
	let message = Promise.resolve("Sorry! You dont have an access.");
	return message.then((value) => {
		return value
	});
}

/* UPDATE PRODUCT */
module.exports.updateProduct = (reqParams, data) => {
	console.log(data.isAdmin);
	if(data.isAdmin){
		let updatedProduct = {
			name: data.product.name,
			description: data.product.description,
			price: data.product.price
		}
		return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((course, error) => {
			if(error){
				return false;
			}else{
				return "Successfully Update the Product";
			}
		})
	}
	let message = Promise.resolve("Sorry! You dont have an access.");
	return message.then((value) => {
		return value
	})
	
}

/* GET ALL PRODUCT */
module.exports.getAllProducts = () => {
	return Product.find({}).then(result => {
		return result;
	});
}

/* GET ONE PRODUCT */
module.exports.getOneProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result =>{
		return result;
	})
}

/* ARCHIVE PRODUCT */
module.exports.archiveProduct = (reqParams, data) => {
	if(data.isAdmin){
		let archivedProduct = {
			isActive: "false"
		}
		return Product.findByIdAndUpdate(reqParams.productId, archivedProduct).then((course, error) => {
			if(error){
				return "Sorry! You dont have an access.";
			}else{
				return "Successfully Deleted the account";
			}
		})
	}
	let message = Promise.resolve("Sorry! You dont have an access.");
	return message.then((value) => {
		return value
	})
	
}