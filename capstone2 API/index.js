const express = require("express");
const mongoose = require("mongoose");

const cors = require("cors");

const productRoutes = require("./routes/product");
const userRoutes = require("./routes/user");
const orderRoutes = require("./routes/order");

const app = express();

mongoose.connect("mongodb+srv://admin:admin123@zuitt-bootcamp.7hg9u9o.mongodb.net/Capstone2_API?retryWrites=true&w=majority",
{
	useNewUrlParser: true,
	useUnifiedTopology: true
});
mongoose.connection.once("open", () => console.log("Now connected to MongoDB Atlas."));

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.use("/products", productRoutes);
app.use("/users", userRoutes);
app.use("/orders", orderRoutes);

if(require.main === module){
	app.listen(process.env.PORT || 4000, () => console.log(`API is now online on port ${process.env.PORT || 4000}`));
}

module.exports = app;